<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('pid');
            $table->string('title');
            $table->string('text');
            $table->timestamps();
        });
        Schema::create('images', function (Blueprint $table) {
            $table->increments('imid');
            $table->string('url',2000);
            $table->integer('pid')->nullable();
            $table->integer('uid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
