<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/welcome','PostsController@index');
Route::get('/posts/create','PostsController@create');
Route::post('/posts','PostsController@store');
Route::get('/postDetail/{pid}','PostsController@postDetail');

Route::post('/postDetail/{pid}/comments','CommentsController@addComment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//functions

Route::get('/search','PublicController@search');
Route::get('/delete-post/{pid}','AdminController@deletePost');
Route::get('/delete-comment/{cid}','AdminController@deleteComment');
Route::get('/delete-user/{uid}','AdminController@deleteUser');
Route::get('logout','HomeController@logout');
Route::post('ask-email','PublicController@askEmail');


//Admin side

Route::get('admin-dashboard','AdminController@adminDashboard');
Route::get('/adminUsersPage','AdminController@adminUsersPage');
Route::get('/adminPostPage/','AdminController@adminPostsPage');
Route::get('/adminCommentsPage','AdminController@adminCommentsPage');




//General

Route::get('dashboard','HomeController@dashboard');






