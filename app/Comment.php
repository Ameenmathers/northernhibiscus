<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected  $table = 'comments';
    protected $primaryKey ='cid';
    protected $guarded =[];

    public function post()
    {
     return $this->belongsTo(Post::class);
    }
    //
}
