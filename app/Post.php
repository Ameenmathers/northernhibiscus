<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected  $table = 'posts';
    protected $primaryKey ='pid';


    public function comments()
    {
      return $this->hasMany(Comment::class,'pid','pid');
    }

    public function addComment($comment)
    {


        Comment::create([

            'comment' => $comment,
            'pid' => $this->pid

        ]);

        return back();
    }

    public function images()
    {
        return $this->hasMany(image::class,'pid','pid');
    }
    //
}
