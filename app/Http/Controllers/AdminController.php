<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\User;
use App\admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\image;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    Public function create(){

        return view('create');

    }

    public function store()
    {
        $post = new Post;
        $post->title = request('title');
        $post->text = request('text');
        $post->save();

        foreach ( $request->file( 'images' ) as $item ) {
            $rand = Str::random(5);
            $inputFileName = $item->getClientOriginalName();
            $item->move("uploads", $rand . $inputFileName);

            $image = new image();
            $image->url = url('uploads/' . $rand . $inputFileName);
            $image->pid = $post->pid;
            $image->uid = Auth::user()->uid;
            $image->save();
        }
        return redirect('/posts/create');
    }

    public function deletePost(Request $request, $pid)
    {
        post::destroy($pid);

        $request->session()->flash('success','Comment Deleted.');
        return redirect('adminPostsPage');
    }

    public function deleteComment(Request $request, $cid)
    {
        comment::destroy($cid);

        $request->session()->flash('success','Comment Deleted.');
        return redirect('adminCommentsPage');
    }

    public function deleteUser(Request $request,$uid)
    {
        user::destroy($uid);

        $request->session()->flash('success','User Deleted.');
        return redirect('adminUsersPage');
    }

    public function adminDashboard()
    {
        $posts = post::all();
        $users = User::all();
        $comments = comment::all();
        return view('admin-dashboard',[
            'posts' => $posts,
            'comments' => $comments,
            'users' => $users

        ]);
    }

    public function adminCommentsPage()
    {

        $comments = comment::all();
        return view('adminCommentsPage',[
            'comments' => $comments

        ]);
    }

    public function adminPostsPage()
    {
        $posts = post::all();
        return view('adminPostsPage',[
            'posts' => $posts
        ]);
    }

    public function adminUsersPage()
    {

        $users = User::all();

        return view('adminUsersPage',[

            'users' => $users,


        ]);
    }
}
