<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostsController extends Controller
{

   Public function index(){

       $posts = post::latest()->get();

       return view('index',[
           'posts' => $posts]);

   }

    public function postDetail($pid)
    {
        $posts = post::where('pid',$pid)->get();


        return view('postDetail',[
            'posts'=> $posts
        ]);

    }

    //
}
