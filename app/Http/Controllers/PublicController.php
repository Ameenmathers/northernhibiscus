<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class PublicController extends Controller
{
    public function index()
    {
        if(!Auth::guest()){
            if(Auth::user()->role == "admin") return redirect('admin-dashboard');
            if(Auth::user()->role == "user") return redirect('welcome');
        }

        $posts = post::latest()->get();
        return view('welcome',[
            'posts' => $posts

        ]);
    }
     //
}
