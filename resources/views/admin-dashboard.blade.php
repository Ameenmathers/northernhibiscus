
<?php use Illuminate\Support\Facades\Input;?>
@extends('layouts.dashboard')




@section('content')

    <!--/.Double navigation-->

    <!--Main Layout-->
    <main>
        <div class="container-fluid mt-5">
            <section class="pb-3">

                <!--Section heading-->
                <h2>Dashboard <i class="fas fa-chevron-right"></i></h2><br>
                <!--Section description-->
                <div class="row">
                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-r">

                        <!-- Card -->
                        <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/pricing-table%20(6).jpg');">

                            <!--Pricing card-->
                            <div class="text-white text-center pricing-card d-flex align-items-center rgba-stylish-strong py-3 px-3">

                                <!--Content-->
                                <div class="card-body">
                                    <h4>Users</h4>
                                    <!--Price-->
                                    <div class="">
                                        <h1>{{count($users)}}<br><i class="fas fa-users fa-2x" style="color:#00C851;"></i></h1>
                                    </div>
                                    <!--Price-->
                                    <a class="btn btn-outline-white" href="{{url('adminUsersPage')}}">View Users</a>
                                </div>

                            </div>
                            <!--Pricing card-->
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-r">

                        <!-- Card -->
                        <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/pricing-table%20(4).jpg');">

                            <!--Pricing card-->
                            <div class="text-white text-center pricing-card d-flex align-items-center rgba-indigo-strong py-3 px-3">

                                <!--Content-->
                                <div class="card-body">
                                    <h4>Posts</h4>
                                    <!--Price-->
                                    <div class="">
                                        <h1>{{count($posts)}}<br><i class="fas fa-inbox fa-2x" style="color:#ffbb33;"></i></h1>
                                    </div>
                                    <!--Price-->
                                    <a class="btn btn-outline-white" href="{{url('adminPostsPage')}}">View Posts</a>
                                </div>

                            </div>
                            <!--Pricing card-->
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-r">

                        <!-- Card -->
                        <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/pricing-table%20(6).jpg');">

                            <!--Pricing card-->
                            <div class="text-white text-center pricing-card d-flex align-items-center rgba-stylish-strong py-3 px-3">

                                <!--Content-->
                                <div class="card-body">
                                    <h4>Comments</h4>
                                    <!--Price-->
                                    <div class="">
                                        <h1>{{count($comments)}}<br><i class="fas fa-comments fa-2x" style="color:#aa66cc;"></i></h1>
                                    </div>
                                    <!--Price-->
                                    <a class="btn btn-outline-white" href="{{url('adminCommentsPage')}}">View Comments</a>
                                </div>

                            </div>
                            <!--Pricing card-->
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
    </main>
    <!--Main Layout-->


@endsection





