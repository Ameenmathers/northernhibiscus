@extends('layouts.app')


@section('content')
<div class="container"><hr>
    <h2>Upload Blog</h2>
    <form method="POST" action="{{url('/posts')}}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" class="form-control" id="title" aria-describedby="title" placeholder="Enter Blog Title">

        </div>
        <div class="form-group">
            <label for="Desc">Text</label>
            <textarea type="text" name="text" class="form-control" id="Desc"></textarea>

        </div>


        <button type="submit"  class="btn btn-primary" >Publish</button>
    </form>
</div>
<br>
@endsection