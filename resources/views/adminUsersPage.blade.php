<?php use Illuminate\Support\Facades\Input;?>
@extends('layouts.dashboard')




@section('content')
<br><br><br><br><br>
        <!--Table-->
<section class="container">

  <h2>Users Table</h2>



    <!--Table-->
    <table class="table">

        <!--Table head-->
        <thead class="mdb-color darken-3">
        <tr class="text-white">
            <th>Name</th>
            <th>Email</th>
            <th>Date Registered</th>
            <th>Username</th>
        </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
        @foreach( $users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td><button href="{{url('deleteUser/' . $user->uid) }}" class="btn peach-gradient btn-rounded"
                            type="submit" name="action">Delete
                        <i class="far fa-trash-alt"></i>
                    </button></td>
            </tr>
        @endforeach


        </tbody>
        <!--Table body-->

    </table>
    <!--Table-->




</section>
<!--Table-->






@endsection