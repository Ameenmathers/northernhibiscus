<?php use Illuminate\Support\Facades\Input;?>
@extends('layouts.dashboard')




@section('content')
    <br><br><br><br><br>
    <!--Table-->
    <section class="container">

        <h2>Posts Table</h2>



        <!--Table-->
        <table class="table">

            <!--Table head-->
            <thead class="mdb-color darken-3">
            <tr class="text-white">
                <th>Post Title</th>
                <th>Post Description</th>
                <th>Date Posted</th>
                <th></th>
            </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
            @foreach( $posts as $post)
                <tr>
                    <td>{{$post->title}}</td>
                    <td>{{$post->text}}</td>
                    <td>{{$post->created_at}}</td>
                    <td><button href="{{url('deletePost/' . $post->pid) }}" class="btn peach-gradient btn-rounded"
                                type="submit" name="action">Delete
                            <i class="far fa-trash-alt"></i>
                        </button></td>
                </tr>
            @endforeach


            </tbody>
            <!--Table body-->

        </table>
        <!--Table-->




    </section>
    <!--Table-->






@endsection