
        {{--<div class="flex-center position-ref full-height">--}}
            {{--@if (Route::has('login'))--}}
                {{--<div class="top-right links">--}}
                    {{--@auth--}}
                        {{--<a href="{{ url('/home') }}">Home</a>--}}
                    {{--@else--}}
                        {{--<a href="{{ route('login') }}">Login</a>--}}
                        {{--<a href="{{ route('register') }}">Register</a>--}}
                    {{--@endauth--}}
                {{--</div>--}}
            {{--@endif--}}

            {{--<div class="content">--}}
                {{--<div class="title m-b-md">--}}
                    {{--Laravel--}}
                {{--</div>--}}

                {{--<div class="links">--}}
                    {{--<a href="https://laravel.com/docs">Documentation</a>--}}
                    {{--<a href="https://laracasts.com">Laracasts</a>--}}
                    {{--<a href="https://laravel-news.com">News</a>--}}
                    {{--<a href="https://forge.laravel.com">Forge</a>--}}
                    {{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <?php use Illuminate\Support\Facades\Input;?>
        @extends('layouts.app')


        @section('content')

            <section class="general-banner banner-layout-one">
                <div class="general-banner-inner">
                    <!-- Swiper -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/one.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">This is featured post</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/two.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">This is uncategorized post</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/three.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">Live wild and free</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/four.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"> <span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">Creat your own happiness</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/five.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">This is featured post</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/six.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">This is featured post</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                            <div class="swiper-slide  banner-card">
                                <div class="featured-post-image">
                                    <img src="img/banner/seven.jpg" alt="....">
                                    <div class="mask"></div>
                                </div>
                                <!-- // featured-post-image -->
                                <div class="featured-content-holder">
                                    <div class="featured-content-meta">
                                        <a href="#"><span class="category">lifestyle</span></a>
                                    </div>
                                    <div class="featured-content-title">
                                        <h2><a href="#">Features in blog</a></h2>
                                    </div>
                                    <div class="featured-posted-date">
                                        <span class="posted-date">17 December, 2017</span>
                                    </div>
                                </div>
                                <!-- // featured-content-holder -->
                            </div>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                        <!-- <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div> -->
                    </div>
                </div>
                <!-- // general banner inner -->
            </section>
            <div class="container">
                <div class="main-post-area-wrapper main-post-area-layout-one">
                    <div class="main-post-area-inner">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div class="main-post-area-holder">

                                    @foreach($posts as $post)
                                    <!-- // post-details-holder -->
                                    <article class="post-details-holder wow  fadeInUp">
                                        <div class="post-image">
                                            <img src="{{$post->Images[0]->url}}" alt="....">
                                        </div>
                                        <!-- // post image -->
                                        <div class="post-title">
                                            <h2>{{$post->title}}</h2>
                                        </div>
                                        <!-- // post-title -->
                                        <div class="post-the-content clearfix layout-one-first-letter">
                                            <p>{{$post->text}} </p>
                                        </div>
                                        <!-- // post-the-content -->
                                        <div class="post-permalink">
                                            <a href="{{url('postDetail/' . $post->pid)}}">Continue Reading</a>
                                        </div>
                                        <!-- // post-permalink -->
                                        <div class="post-meta-and-share">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="post-author">
                                                        <span class="post-comment-count"><a href="#">{{$post->created_at->toFormattedDateString()}}</a></span>
                                                    </div>
                                                </div>
                                                <!-- // col 4 -->
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="post-share">
                                                        <div class="share"></div>
                                                    </div>
                                                </div>
                                                <!-- // col 4 -->
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <div class="post-comment-count">
                                                        <span class="post-comment-count"><a href="#">17 Comment</a></span>
                                                    </div>
                                                </div>
                                                <!-- // col 4 -->
                                            </div>
                                            <!-- // row -->
                                        </div>
                                    </article>
                                    <!-- // post-details-holder -->
                                     @endforeach
                                </div>
                                <!-- // main-post-area-holder -->
                            </div>
                            <!-- // col -->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                                <aside class="sidebar">
                                    <div class="sidebar-inner">

                                        <!-- // widget -->
                                        <div class="widget widget-social-links wow fadeInUp">
                                            <div class="widget-content">
                                                <div class="widget-title">
                                                    <h2>I'M social</h2>
                                                </div>
                                                <div class="widget-extra-info-holder">
                                                    <div class="widget-social-links">
                                                        <ul class="social-links-list">
                                                            <li class="facebook-link">
                                                                <a href="http://facebook.com/" class="clearfix" target="_blank">
                                                                    Facebook
                                                        <span class="social-icon">
                                                        <i class="fa fa-facebook"></i>
                                                         </span>
                                                                </a>
                                                            </li>
                                                            <li class="twitter-link">
                                                                <a href="http://twitter.com/" class="clearfix" target="_blank">
                                                                    Twitter
                                                         <span class="social-icon">
                                                                <i class="fa fa-twitter"></i>
                                                         </span>
                                                                </a>
                                                            </li>
                                                            <li class="googleplus-link">
                                                                <a href="http://plus.google.com/" class="clearfix" target="_blank">
                                                                    Google Plus
                                                            <span class="social-icon">
                                                                <i class="fa fa-google-plus"></i>
                                                            </span>
                                                                </a>
                                                            </li>
                                                            <li class="instagram-link">
                                                                <a href="http://instagram.com/" class="clearfix" target="_blank">
                                                                    Instagram
                                                             <span class="social-icon">
                                                                <i class="fa fa-instagram"></i>
                                                                 </span>
                                                                </a>
                                                            </li>
                                                            <li class="linkedin-link">
                                                                <a href="http://linkedin.com/" class="clearfix" target="_blank">
                                                                    Linked In
                                                            <span class="social-icon">
                                                             <i class="fa fa-linkedin"></i>
                                                            </span>
                                                                </a>
                                                            </li>
                                                            <li class="youtube-link">
                                                                <a href="http://youtube.com/" class="clearfix" target="_blank">
                                                                    Youtube
                                                        <span class="social-icon">
                                                             <i class="fa fa-youtube"></i>
                                                         </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // widget-extra-info-holder -->
                                            </div>
                                            <!-- // widget-content -->
                                        </div>
                                        <!-- // widget -->

                                        <div class="widget widget-recent-posts wow fadeInUp">
                                            <div class="widget-content">
                                                <div class="widget-title">
                                                    <h2>Recent posts</h2>
                                                </div>
                                                <div class="widget-extra-info-holder">
                                                    <div class="widget-recent-posts">
                                                        <div class="widget-rpag-gallery-container">
                                                            <div class="swiper-wrapper">
                                                                <div class="swiper-slide">
                                                                    <img src="img/post-five.jpg" alt="...">
                                                                    <div class="mask"></div>
                                                                    <div class="slide-content">
                                                                        <div class="post-title">
                                                                            <h5><a href="#">That Evening At Bali Beach Was Wounderful Then Any Other Mornings</a></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="swiper-pagination"></div>
                                                        </div>
                                                        <!-- // widget-rpag-gallery-container -->
                                                    </div>
                                                    <!--// widget-recent-posts -->
                                                </div>
                                                <!-- // widget-extra-info-holder -->
                                            </div>
                                            <!-- // widget-content -->
                                        </div>
                                        <!-- // widget widget-newsletter -->

                                        <div class="widget widget-category wow fadeInUp">
                                            <div class="widget-content">
                                                <div class="widget-title">
                                                    <h2>Category</h2>
                                                </div>
                                                <div class="widget-extra-info-holder">
                                                    <ul class="widget-category-listings">
                                                        <li><a href="#">Lifestyle</a></li>
                                                        <li><a href="#">Health</a></li>
                                                        <li><a href="#">Article</a></li>
                                                        <li><a href="#">Travel</a></li>
                                                        <li><a href="#">Uncategorised</a></li>
                                                    </ul>
                                                </div>
                                                <!-- // widget-extra-info-holder -->
                                            </div>
                                            <!-- // widget-content -->
                                        </div>
                                        <!-- // widget widget-category -->
                                        <div class="widget widget-popular-post wow fadeInUp">
                                            <div class="widget-content">
                                                <div class="widget-title">
                                                    <h2>Trending Posts</h2>
                                                </div>
                                                <div class="widget-extra-info-holder">
                                                    <div class="widget-posts">
                                                        <div class="post-thumb">
                                                            <img src="img/thumb/one.jpg" alt=".....">
                                                        </div>
                                                        <div class="post-title">
                                                            <h5><a href="#">That Evening At Bali Beach Was Wounderful Then Any Other Mornings</a></h5>
                                                        </div>
                                                        <div class="post-view-count post-meta">
                                                            <p>444 <span>comments</span></p>
                                                        </div>
                                                    </div>

                                                    <!-- // widget-post -->
                                                </div>
                                                <!-- // widget-extra-info-holder -->
                                            </div>
                                            <!-- // widget-content -->
                                        </div>
                                        <!-- // widget -->
                                        <div class="widget widget-newsletter wow fadeInUp">
                                            <div class="widget-content">
                                                <div class="widget-title">
                                                    <h2>Newsletter</h2>
                                                </div>
                                                <div class="widget-extra-info-holder">
                                                    <div class="widget-newsletter-content">
                                                        <p>Subscribe to newsletter to be updated with all the latest trends and products.</p>
                                                        <form>
                                                            <div class="form-group">
                                                                <input type="email" name="newsletter-email" placeholder="Email Address ..." class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit" name="submit" class="layout-one-btn">Subscribe</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--// widget-newsletter-content -->
                                                </div>
                                                <!-- // widget-extra-info-holder -->
                                            </div>
                                            <!-- // widget-content -->
                                        </div>
                                        <!-- // widget widget-newsletter -->

                                    </div>
                                    <!-- // sidebar-inner -->
                                </aside>
                                <!-- // sidebar -->
                            </div>
                            <!-- // col 4 -->
                        </div>
                        <!-- // main row that divies left and sidebar -->
                        <div class="pagination_holder">
                            <ul class="pagination">
                                <li class="disabled"><a href="#">�</a></li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">�</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- // main-post-area-inner -->
                </div>
                <!-- // main-post-area-wrapper main-post-area-layout-one -->
            </div>
        @endsection






