
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Northern Hibiscus</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,600,700,700i&amp;subset=cyrillic,greek-ext,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Stalemate&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Spectral+SC:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
    <link href="{{url('css/style.min.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<header class="general-header header-layout-one">
    <div class="general-header-inner">
        <div class="header-top-wrapper">
            <div class="header-top-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="search">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                <input type="search" placeholder="Search ..........">
                                @if(!Auth::guest())
                                    @if(Auth::user()->role == 'admin')
                                        <a class="btn btn-elegant" href="{{ url('dashboard') }}"><i class="fa fa-user-o pr-2" aria-hidden="true">Dashboard</i></a>
                                    @endif
                                    <a href="#" class="btn btn-elegant" >Logged in as {{Auth::user()->fname}} {{Auth::user()->lname}}<i class="fa fa-user-o pr-2"></i></a>
                                    <a href="{{url('logout')}}" class="btn btn-elegant">Logout</a>
                                @else
                                    <a href="{{ url('login') }}" class="btn btn-elegant">Sign In</a>
                                    <a href="{{ url('register') }}" class="btn btn-elegant">Register</a>
                                @endif
                            </div>
                        </div>
                        <!-- // col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="social-networks">
                                <ul class="social-links">
                                    <li><a href="facebook.com"></a></li>
                                    <li><a href="twitter.com"></a></li>
                                    <li><a href="instagram.com"></a></li>
                                    <li><a href="youtube.com"></a></li>
                                    <li><a href="snapchat.com"></a></li>
                                </ul>
                            </div>
                            <!-- // social-networks -->
                        </div>
                        <!-- // col -->
                    </div>
                    <!-- // row -->
                </div>
                <!-- // container -->
            </div>
            <!-- // header-top-inner -->
        </div>
        <!-- // header-top-wrapper -->
        <div class="container">
            <div class="site-info">
                <h1 class="site-title">Northern hibiscus</h1>
                <!--  <img src="./assets/dist/img/logo.png" alt="logo"> -->
            </div>
            <!-- // site-info -->
        </div>
        <!-- // container -->
        <nav class="main-nav layout-one">
            <div id="main-nav" class="stellarnav">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Lifestyle</a></li>
                    <li><a href="#">Weddings</a></li>
                    <li><a href="#">Amana</a></li>

                </ul>
            </div>
            <!-- .stellar-nav -->
        </nav>
    </div>
    <!-- // general-header-inner -->
</header>



