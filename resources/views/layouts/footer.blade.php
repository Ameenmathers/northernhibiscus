<div id="instafeed" class="instafeed owl-carousel feed-carousel">

</div>

<footer class="general-footer" style="background-image:url(assets/dist/img/footer-bg7.jpg)">

    <div class="footer-mask"></div>

    <div class="footer-inner">

        <div class="container">

            <div class="row">

                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 footer-block">

                    <div class="">

                        <div class="footer-widget-content about-widget">

                            <div class="widget-title">

                                <h2>About</h2>

                            </div>

                            <div class="widget-about-site-logo">

                                <span>Northern Hibiscus</span>

                            </div>

                            <div class="widget-content">

                                <p>A Northern Girl's View</p>

                            </div>

                            <!-- // widget-content -->

                            <div class="social-networks">

                                <ul class="social-links">

                                    <li><a href="https://facebook.com/"></a></li>

                                    <li><a href="https://twitter.com/"></a></li>

                                    <li><a href="https://instagram.com/"></a></li>

                                    <li><a href="https://youtube.com/"></a></li>

                                    <li><a href="https://snapchat.com/"></a></li>

                                </ul>

                            </div>

                        </div>

                        <!-- // footer-widget-content -->

                    </div>

                    <!-- // footer block -->

                </div>

                <!-- // col -->

                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 footer-block">

                    <div class="">

                        <div class="footer-widget-content">

                            <div class="widget-title">

                                <h2>Navigation</h2>

                            </div>

                            <div class="widget-content">

                                <ul class="widget-category-listings">

                                    <li><a href="#">Lifestyle</a></li>

                                    <li><a href="#">Cooking with HM</a></li>

                                    <li><a href="#">Weddings</a></li>

                                    <li><a href="#">Uncategorised</a></li>

                                </ul>

                            </div>

                            <!-- // widget-content -->

                        </div>

                        <!-- // footer-widget-content -->

                    </div>

                    <!-- // footer block -->

                </div>

                <!-- // col -->

                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 footer-block">

                    <div class="">

                        <div class="footer-widget-content">

                            <div class="widget-title">

                                <h2>Popular Posts</h2>

                            </div>

                            {{--<div class="widget-content">--}}
                                {{--@foreach($posts as $post)--}}
                                {{--<div class="widget-posts">--}}

                                    {{--<div class="post-title">--}}

                                        {{--<h5><a href="#">{{$post->title}}</a></h5>--}}

                                    {{--</div>--}}

                                    {{--<div class="posted-date">--}}

                                        {{--<span><a href="#">{{$post->created_at->toFormattedDateString()}}</a></span>--}}

                                    {{--</div>--}}

                                {{--</div>--}}
                                {{--@endforeach--}}

                                {{--<!-- // widget-post -->--}}

                            {{--</div>--}}

                            <!-- // widget-content -->

                        </div>

                        <!-- // footer-widget-content -->

                    </div>

                    <!-- // footer block -->

                </div>

                <!-- // col -->

            </div>

            <!-- // row -->

            <div class="copyright-and-nav-row">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <div class="copyrights">

                            <p>Copyrights @ 2018 Matherly</p>

                        </div>

                        <!-- // copyrights -->

                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <ul class="footer-navigation">

                            <li><a href="#">Lifestyle</a></li>

                            <li><a href="#">Wedding</a></li>

                            <li><a href="#">Amana</a></li>

                            <li><a href="#">Cooking with HM</a></li>

                        </ul>

                    </div>

                </div>

                <!-- // row -->

            </div>

            <!-- // copyright-and-nav-row -->

        </div>

        <!-- // container -->

    </div>

    <!-- // footer inner -->

</footer>

<script src="{{url('js/bundle.min.js')}}"></script>

<script src="{{url('js/scripts.js')}}"></script>

</body>
</html>
